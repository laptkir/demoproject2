//
//  BaseView.swift
//  DemoProject2
//
//  Created by Cyril on 25.10.21.
//

import UIKit

class BaseView: UIView {
    
    @IBOutlet weak var warningLabel: UILabel?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var personExclamationView: CustomView!
    @IBOutlet weak var flagView: CustomView!
    @IBOutlet weak var locationView: CustomView!
    
    var labelsData: Model? {
        didSet {
            warningLabel?.text = labelsData?.warningString
            titleLabel?.text = labelsData?.titleString
            button.backgroundColor = .white
            button.layer.cornerRadius = 11
            
            if let customView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as? CustomView {
                self.personExclamationView.addSubview(customView)
                customView.label.text = labelsData?.personExclamationString
                customView.imageView.image = UIImage(systemName: labelsData?.personImageName ?? "")
                customView.imageView.tintColor = .white
                }
            
            if let customView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as? CustomView {
                self.flagView.addSubview(customView)
                customView.label.text = labelsData?.flagString
                customView.imageView.image = UIImage(systemName: labelsData?.flagImageName ?? "")
                customView.imageView.tintColor = .white
                }
            
            if let customView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as? CustomView {
                self.locationView.addSubview(customView)
                customView.label.text = labelsData?.locationString
                customView.imageView.image = UIImage(systemName: labelsData?.locationImageName ?? "")
                customView.imageView.tintColor = .white
                }
        }
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
    }
}
