//
//  CustomView.swift
//  DemoProject2
//
//  Created by Cyril on 26.10.21.
//

import UIKit

class CustomView: UIView {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
