//
//  ViewController.swift
//  DemoProject2
//
//  Created by Cyril on 25.10.21.
//

import UIKit

class BaseController: UIViewController {
    
    var modelFether = ModelFecther()
    var model: Model?
    
    var baseView: BaseView! {
        guard isViewLoaded else { return nil }
        return (view as! BaseView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseView.labelsData = modelFether.purpleModel
        if model != nil {
            baseView.labelsData = model
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowVC" {
            if let destinationVC = segue.destination as? BaseController {
                if baseView.labelsData == modelFether.purpleModel {
                    destinationVC.model = modelFether.customPurpleModel
                } else {
                    dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}


