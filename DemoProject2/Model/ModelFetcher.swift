//
//  ModelFetcher.swift
//  DemoProject2
//
//  Created by Cyril on 25.10.21.
//

import Foundation
import UIKit

final class ModelFecther {
    
    let purpleModel = Model(personExclamationString: "Alerts when your pals are nearby", flagString: "News of events happening near you", locationString: "Tagging and sharing your location", warningString: "You can change this later in Settings App.", titleString: "Turning on location services allows us to provide features like:", personImageName: "person.crop.circle.badge.exclamationmark", flagImageName: "flag.fill", locationImageName: "location.circle")
    
    let customPurpleModel = Model(personExclamationString: "Special offers and promotions just for you", flagString: "Advertisments that match your interest", locationString: "An improved personalized experience over time", warningString: "You can change this option later in Settings App.", titleString: "Allow tracking on the next screen for:", personImageName: "heart.circle.fill", flagImageName: "hand.tap.fill", locationImageName: "chart.bar.fill")
    
}
