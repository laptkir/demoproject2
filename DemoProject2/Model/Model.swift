//
//  Model.swift
//  DemoProject2
//
//  Created by Cyril on 25.10.21.
//

import Foundation
import UIKit

struct Model: Equatable {
    
    let personExclamationString: String
    let flagString: String
    let locationString: String
    let warningString: String
    let titleString: String
    let personImageName: String
    let flagImageName: String
    let locationImageName: String
}
